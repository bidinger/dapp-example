# Atomic Swap Dapp

This is a simple Dapp example using the
[ConseilJS](https://cryptonomic.github.io/ConseilJS/) library. Currently,
it is just a webpage which calls a contract transfering 1 mutez to the
specified address. Eventually, this should become an atomic swap dapp.

## Installation

The contract can be originate using the `originate contract` command of the
`tezos-client`.  We use here `zeronet` which runs the Babylon protocol. The
command given here needs to be adjusted if using Athens.

```
MY_ACCOUNT=tz1bwT9XB9tpzTKKw7UtTSzS9dfXjAzqXzoi

> ~/tezos-zeronet/tezos-client originate contract atomic_swap transferring 10 \
 from $MY_ACCOUNT running ./atomic_swap.tz --init Unit --burn-cap 10.0 --force

Node is bootstrapped, ready for injecting operations.
Estimated gas: 11702 units (will add 100 for safety)
Estimated storage: 320 bytes added (will add 20 for safety)
Operation successfully injected in the node.
Operation hash is 'oo6d6jhJouLjabzDBaJt4dRJqGxoZPzuQiefBMC2szujtpyX4cP'
Waiting for the operation to be included...
Operation found in block: BL362riCrX5uyVrZAm9hDeq8uN4FMiVLoAQfAhRAETMVttUbNGZ (pass: 3, offset: 0)

...

New contract KT1RW6RhD88q8BgkfET64PGRefNp4F9inTu9 originated.
The operation has only been included 0 blocks ago.
We recommend to wait more.
Use command
  tezos-client wait for oo6d6jhJouLjabzDBaJt4dRJqGxoZPzuQiefBMC2szujtpyX4cP to be included --confirmations 30 --branch BKiPY2YjP1fQ2s8mh6v7GCHwMFf6zPXmp9g7aebkrL5ohnZk3do
and/or an external block explorer.
Contract memorized as atomic_swap.
```

The client returns various information, including the contract address
`KT1RW6RhD88q8BgkfET64PGRefNp4F9inTu9`.

The JS script in `index.html` needs to be updated with this contract address,
as well as the address of the node the contract is interacting with.

```
const server = 'http://127.0.0.1'
const contract_address = 'KT1RW6RhD88q8BgkfET64PGRefNp4F9inTu9'
```
